# all: sfml-gui

# WARNINGS = -Wall
# DEBUG = -ggdb -fno-omit-frame-pointer -fconcepts-ts
# OPTIMIZE = -O2
# LIBS = -lsfml-graphics -lsfml-window -lsfml-system

# sfml-gui: Makefile src/main.cpp
# 	g++ -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) $(LIBS) src/main.cpp src/componentes/*.cpp

# clean:
# 	rm -f sfml-gui

# Builder will call this to install the application before running.
# install:
# 	echo "Installing is not supported"

# Builder uses this target to run your application.
# run:
# 	./sfml-gui


all: build

build:
	mkdir -p build
	g++ src/componentes/button.cpp -c -o build/button.o && ar crv build/libbutton.a build/button.o
	g++ src/componentes/window.cpp -c -o build/window.o && ar crv build/libwindow.a build/window.o
	g++ src/componentes/checkbox.cpp -c -o build/checkbox.o && ar crv build/libcheckbox.a build/checkbox.o

install:
	cp build/*.a /usr/lib64/
	mkdir -p /usr/include/sfml-gui
	cp src/componentes/*.h /usr/include/sfml-gui/

clean:
	rm -rf build

uninstall:
	sudo rm -rf /usr/include/sfml-gui




# PARA COMPILAR UMA BIBLIOTECA
# g++ button.cpp -c
# ar crv libbutton.a button.o
# (opcional) mover para /usr/lib64
#
# PARA INCLUIR A BIBLIOTECA
# g++ main.cpp -lsfml-graphics -lsfml-window -lsfml-system -lbutton
#

