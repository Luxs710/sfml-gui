#include <SFML/Graphics.hpp>
#include "checkbox.h"

gCheckbox::gCheckbox(float s, float x, float y){
    size = s;
    posX = x;
    posY = y;

    box.setSize(sf::Vector2f(s,size));
    box.setFillColor(sf::Color(0,0,0,0));
    box.setOutlineColor(sf::Color(0,255,0));
    box.setOutlineThickness(2);
    box.setPosition(x,y);

}
